#!/usr/bin/env bash

CLUSTER_NAME="PRODUCTION"
TAG_NAME=k3s_cluster
#KUBECTL_VERSION=1.16.2

# Verify that doctl is configured for use.
doctl account get 1> /dev/null
if [[ $? != 0 ]]; then
  echo "This script relies on the digitalocean cli (doctl)"
  echo "Please install and configure doctl."
  echo "https://github.com/digitalocean/doctl#installing-doctl"
  exit 1
fi

# Check that the user has exported an SSH key fingerprint.
if [ -z ${DO_SSH_KEY+x} ]; then
  echo "Please set the environment variable DO_SSH_KEY to a public"
  echo "key fingerprint that has been uploaded to DigitalOcean."
  echo "https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/to-account/"
  exit 1
fi

# Ensure that there are no existing nodes tagged for our cluster.
if [[ $(doctl compute droplet list --format "PublicIPv4" --no-header --tag-name "${TAG_NAME}" | wc -l) -gt 0 ]] ; then
  echo "Existing nodes found with the tag ${TAG_NAME}."
  echo "Delete them before continuing"
  exit 1
fi

# Create our tag if it doesn't exist
doctl compute tag create ${TAG_NAME} 1> /dev/null

echo "Building digitalocean virtual machines..."
for node in kube-n01 kube-n02 kube-n03; do
	doctl compute droplet create ${node} \
		--size s-1vcpu-1gb \
		--image ubuntu-18-04-x64 \
		--region nyc1 \
		--ssh-keys ${DO_SSH_KEY} \
		--enable-private-networking \
		--tag-name "${TAG_NAME}" \
    --format "Name,PublicIPv4,PrivateIPv4" \
		--wait
done

declare -a PUBLIC_IPS=($(doctl compute droplet list --format "PublicIPv4" --no-header --tag-name ${TAG_NAME} | tac))
declare -a PRIVATE_IPS=($(doctl compute droplet list --tag-name ${TAG_NAME} --no-header --format "PrivateIPv4" | tac))

#CONTROL_PLANE=$(echo "${PUBLIC_IPS}" | head -1)

# Update the DNS record for kube.homeskillet.org
#echo "Updating kube.homeskillet.org DNS record to ${PUBLIC_IPS[0]}"
#doctl compute domain records update homeskillet.org --record-id=89006212 --record-data="${PUBLIC_IPS[0]}" 1> /dev/null

echo "Verifying that there are exactly 3 nodes for our cluster..."
if [[ ${#PUBLIC_IPS[@]} -ne 3 ]] ; then
  "There are not exactly four nodes tagged for this cluster. exiting."
  exit 1
fi

echo "Creating ansible inventory.ini"

# Create an ansible inventory file with our new servers.
cat > inventory.ini <<EOL

[all]
kube-n01 ansible_host=${PUBLIC_IPS[0]}
kube-n02 ansible_host=${PUBLIC_IPS[1]}
kube-n03 ansible_host=${PUBLIC_IPS[2]}

# ## configure a bastion host if your nodes are not directly reachable
# bastion ansible_host=x.x.x.x ansible_user=some_user

[master]
kube-n01

[worker]
kube-n02
kube-n03

[all:vars]
ansible_connection=ssh
ansible_ssh_user=root
ansible_ssh_common_args='-o StrictHostKeyChecking=no'
command_warnings=False
k3s_url="https://${PRIVATE_IPS[0]}:6443"
k3s_endpoint="https://${PUBLIC_IPS[0]}:6443"
EOL
