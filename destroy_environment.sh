#!/usr/bin/env bash
TAG_NAME=k3s_cluster

NODE_IDS=$(doctl compute droplet list --tag-name ${TAG_NAME} --no-header --format "ID")
for id in ${NODE_IDS} ; do
  doctl compute droplet delete ${id} --force
done