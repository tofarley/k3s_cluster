# README
This project is intended for demonstration puposes. It will build a three-node kubernetes cluster running k3s on a single control plane node using docker. The nodes will be Ubuntu 18.04 virtual machines on DigitalOcean (DO).

## Prerequisites
Everything in this repository is designed to be run from your local workstation or virtual machine, so you will need to install the following utilities before you begin. The following versions have been tested:

```
ansible     = 2.9.4
curl        = 7.58.0
python3     = 3.6.9
python3-pip = 9.0.1
```

The following will install `python3` and `python3-pip` system-wide and set them as default. You may prefer to install them into a virtual environment. That is left as an exercise to the user.

It is highly recommended that you install ansible via pip, rather than from your distribution's package manager.
As of this writing, the ansible version in pypi is `2.9.4`, while the version in the ubuntu 18.04 repository is at `2.5.1`. Your mileage may vary.

### Ubuntu/Debian
```
sudo apt update
sudo apt install -y curl python3 python3-pip bash-completion
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 10
sudo update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 10
sudo pip install ansible
```

### RHEL/CentOS
```
sudo yum install -y curl python3 python3-pip bash-completion
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 10
sudo update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 10
sudo pip install ansible
```

## DigitalOcean
Generate a DigitalOcean API token:
* Navigate in the DO portal to `Manage \ API \ Tokens/Keys`.
* Grant the token both read and write access.
* export your new token in your `~/.bashrc` file as `DO_API_KEY`.

Upload your SSH public key to DigitalOcean:
* Navigate in the DO portal to `Account \ Security`.
* Export the fingerprint of this newly added key in your `~/.bashrc` file as `DO_SSH_KEY`.

This key will be used for ssh access to each of your nodes, and for ansible provisioning.

## Node requirements
At the time of this writing, Ubuntu 16/18 are the only officially supported operating systems for k3s on amd64 architecture.


* Ununtu 16.04 / 18.04
* Docker 19.03.x

See [Node Requirements](https://rancher.com/docs/k3s/latest/en/installation/node-requirements/) for additional details.

## Installation
Run the following script to build a cluster of nodes on digitalocean tagged with `k3s_cluster`. The script will also generate an ansible `inventory.ini` file for our cluster.
```
./build_environment.sh
```

The ansible playbook uses an encrypted vault value `k3s_token` stored in vars.yaml. If you wish to use the provided token, you must pass the `--ask-vault-pass` parameter when executing the playbook.

Alternatively, you can specify your own value for `k3s_token`. This value is used when registering a new cluster and required by each node who wishes to join the cluster. You can therefore set this value to any reasonable value.
```
ansible-playbook -i inventory.ini install_k3s.yml --ask-vault-pass
```

## Accessing your cluster
You can access the cluster by connecting to the master node via ssh and executing the `k3s` command directly, however you will most often likely want to connect remotely using `kubectl`.

The ansible playbook will download a local copy of the kubectl config file from the master and place in the current directory as `k3s.yaml`. Install kubectl and export the path to this file as `KUBECONFIG` to begin using the cluster!
```
sudo curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.17.0/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl && sudo chmod +x /usr/local/bin/kubectl
export KUBECONFIG=${PWD}/k3s.yaml
```
